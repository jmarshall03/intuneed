'use strict'

const Lucid = use('Lucid')

class Period extends Lucid {

    students(){
        return this.hasMany('App/Model/Students')
    }
    assignmentLists(){
        return this.hasMany('App/Model/AssignmentList')
    }
}

module.exports = Period
