'use strict'

const Lucid = use('Lucid')

class Students extends Lucid {
    
    period() {
        return this.belongsTo('App/Model/Period')
    }
    
    studentsAssignment(){
        return this.belongsTo('App/Model/StudentAssignment')
    }

}

module.exports = Students
