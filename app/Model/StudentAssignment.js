'use strict'

const Lucid = use('Lucid')

class StudentAssignment extends Lucid {
    assignmentList() {
        return this.belongsTo('App/Model/AssignmentList')
    }
}

module.exports = StudentAssignment
