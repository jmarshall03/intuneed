'use strict'

const Lucid = use('Lucid')

class AssignmentList extends Lucid {
    
    static get rules () { 
        return {
          assignment_lists: 'unique:assignment_name'
          /*type: 'required|unique:assignment_name,period_id,teacher',
          teacher: 'required|unique:assignment_name,period_id,teacher'*/
        }
    }
    
    studentAssignments(){
        return this.hasMany('App/Model/StudentAssignment')
    }
    
    periodAssignments(){
        return this.belongsTo('App/Model/Period')
    }
    
    
}


module.exports = AssignmentList
