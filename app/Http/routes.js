'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.get('/', 'PeriodController.index')
Route.resources('period', 'PeriodController')
Route.resources('period.assignments', 'AssignmentListController')
Route.resources('period.assignments.assignment', 'StudentAssignmentController')
//Route.on('/period/:id').render('about')
//Route.on('/').render('layouts.main')
/*Route.get('/', function * (req, res) {
  res.send('This is the home page')
})*/

/*
Route.get('/:name?', function * (req, res) {
    const name = req.param('name', 'newcomer')
    res.send(`This is the home page for ${name}`)
})
*/




