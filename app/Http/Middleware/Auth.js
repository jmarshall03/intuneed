'use strict'

class Auth {

  * handle (req, res, next) {
      
      const name = req.param('name','foo');
      
      if(name !== 'Jordin'){
          res.status(401).send('Not Allowed')
      }
      
      yield next
    // here goes your middleware logic
    // yield next to pass the request to next middleware or controller
    //yield next
  }

}

module.exports = Auth
