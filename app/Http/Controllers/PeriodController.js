'use strict'
const Database = use('Database')
const Period = use('App/Model/Period')
const AssignmentList = use('App/Model/AssignmentList')


class PeriodController {
    
/*********************************************************

    Index

*********************************************************/
    * index(req,res){
        const periods = yield Period.all()
        yield res.sendView('period.index', { periods: periods.toJSON() })
    }
    
    * create(req, res) {
        const periodId = req.param('id')
        try{
            const period = yield Period.findOrFail(periodId)
        }
        catch(e){
            yield res.sendView('errors.index')
        }
        yield res.sendView('createPost')
    }

/*********************************************************

    Store

*********************************************************/
    * store(req, res) {
    // 
    }

/*********************************************************

    Show

*********************************************************/
    * show(req, res,id) {
        const periods = yield Period.all()
        const periodId = req.param('id')
        try{
            const period = yield Period.findOrFail(periodId)
        }
        catch(e){
            yield res.sendView('errors.index')
        }
        const period = yield Period.findOrFail(periodId)
        const lists = yield AssignmentList.query().where('period_id', periodId).fetch()
        yield res.sendView('period.show', {lists: lists.toJSON() , periodId:periodId, periods: periods.toJSON()})                      
    }
    
/*********************************************************

    Edit

*********************************************************/

    * edit(req, res) {
    //
    }
    
/*********************************************************

    Update

*********************************************************/    
    * update(req, res) {
    //
    }
    
/*********************************************************

    Destroy

*********************************************************/
    * destroy(req, res) {
    //
    }

}

module.exports = PeriodController
