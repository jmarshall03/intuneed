'use strict'

const Database = use('Database')
const Validator = use('Validator')
const Period = use('App/Model/Period')
const Students = use('App/Model/Students')
const AssignmentList = use('App/Model/AssignmentList')
const StudentAssignment = use('App/Model/StudentAssignment')

class StudentAssignmentController {

  * index(req, res) {
    //
  }

  * create(req, res) {
        const periods = yield Period.all()
        const periodId = req.param('period_id')
        const assignId = req.param('assignments_id')
        const assignment = yield AssignmentList.findOrFail(assignId)
        const students = yield Students.query().where('period_id',periodId).orderBy('student_name', 'asc').fetch()
        yield res.sendView('assignment.create', { periodId:periodId,assignId:assignId, students: students.toJSON(),  periods: periods.toJSON(),assignment: assignment.toJSON() })

  }

  * store(req, res) {
        const periods = yield Period.all()
        const periodId = req.param('period_id')
        const assignId = req.param('assignments_id')
        const assignment = yield AssignmentList.findOrFail(assignId)
        const lists = yield AssignmentList.query().where('id', assignId).fetch()
        const data = req.only('assignment_list_id')
        const assigned = req.param('assignment_list_id')

        const studentAssignments = req.collect('student_name', 'assignment_name', 'period_id', 'assignment_list_id','student_id', 'teacher', 'assignment_type', 'complete_status')
        const savedAssignment = yield StudentAssignment.createMany(studentAssignments)

        yield res.sendView('assignments.index', { periodId:periodId, assignId:assignId, id:assignId, periods: periods.toJSON(),lists: lists.toJSON(), assignment: assignment.toJSON() })
  }

  * show(req, res) {
    //
  }

  * edit(req, res) {
        const periods = yield Period.all()
        const assignmentId = req.param('id')
        const periodId = req.param('period_id')
        const assignId = req.param('assignments_id')
        const assignment = yield AssignmentList.findOrFail(assignId)

        const assignmentData = yield StudentAssignment.query().where('assignment_list_id',assignmentId).orderBy('student_name', 'asc').fetch()

        yield res.sendView('assignment.edit', {periods: periods.toJSON(), assignmentData: assignmentData.toJSON(), periodId:periodId, assignId:assignId, assignment:assignment})
  }

  * update(req, res) {
        const studentAssignmentId = req.param('id')
        const periodId = req.param('period_id')
        const assignId = req.param('assignments_id')
        const assignmentData = req.collect('assignment_name', 'assignment_type','teacher', 'student_name', 'complete_status', 'student_id', 'assignment_list_id')
        
        for (var i = 0; i < assignmentData.length; i++) {
            const data = assignmentData[i];
            const studentAssignment = yield StudentAssignment.findBy('student_id', data.student_id)
            const dataS =  req.only('complete_status','assignment_name')
            //res.json(dataS.assignment_name[i])
            yield Database.table('student_assignments')
                .where({'assignment_list_id': assignId, 'student_id': data.student_id})
                .update('complete_status', dataS.complete_status[i])
            
        }
     res.route('period.assignments.show', {period_id:periodId, id:assignId})
  }

  * destroy(req, res) {
    const periods = yield Period.all()
    const assignmentId = req.param('id')
    const periodId = req.param('period_id')
    const assignId = req.param('assignments_id')
    const assignment = yield AssignmentList.findOrFail(assignId)
    const lists = yield AssignmentList.query().where('id', assignId).fetch()
    const assignmentDelete = yield StudentAssignment.findBy('assignment_list_id',assignmentId)
    yield assignmentDelete.delete()
    yield res.sendView('assignments.index', { periodId:periodId, assignId:assignId, id:assignId, periods: periods.toJSON(),lists: lists.toJSON()})
  }

}

module.exports = StudentAssignmentController