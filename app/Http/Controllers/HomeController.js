'use strict'

class HomeController {

  * index(req, res) {
    const view = yield res.view('index')
    res.ok(view)
  }
    
  *greet(req,res){
      const name = req.param('name', 'newcomer')
      res.send(`This is the home page for ${name}`)
  }
    
  *about(req,res){
      res.send('We are awesome')
  }

  * create(req, res) {
    //
  }

  * store(req, res) {
    //
  }

  * show(req, res) {
    //
  }

  * edit(req, res) {
    //
  }

  * update(req, res) {
    //
  }

  * destroy(req, res) {
    //
  }

}

module.exports = HomeController
