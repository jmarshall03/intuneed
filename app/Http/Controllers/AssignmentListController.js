'use strict'
const Database = use('Database')
const Validator = use('Validator')
const Period = use('App/Model/Period')
const Students = use('App/Model/Students')
const AssignmentList = use('App/Model/AssignmentList')
const StudentAssignment = use('App/Model/StudentAssignment')
class AssignmentListController {
    
/*********************************************************

    Index

*********************************************************/

  * index(req, res) {
        const periods = yield Period.all()
        const periodId = req.param('period_id')
        const assignId = req.param('assignment_id')
        const period = yield Period.findOrFail(periodId)
        const lists = yield AssignmentList.query().where('period_id', periodId).fetch()
        yield res.sendView('assignments.index', {lists: lists.toJSON(), periods: periods.toJSON(),periodId:periodId, assignId:assignId })
  }
/*********************************************************

        Create

*********************************************************/
 
  * create(req, res) {
      const periods = yield Period.all()
      const periodId = req.param('period_id')
      const assignId = req.param('assignment_id')
        try{
            const period = yield Period.findOrFail(periodId)
        }
        catch(e){
            yield res.sendView('errors.index')
        }
        const period = yield Period.findOrFail(periodId)
      yield res.sendView('assignments.create', { periods: periods.toJSON(), period: period.toJSON(), assignId:assignId })
  }
    
/*********************************************************

    Store

*********************************************************/
  * store(req, res) {
      const periodId = req.param('period_id')
      const period = yield Period.findOrFail(periodId)
      const assignmentData = req.only('assignment_name', 'type','teacher') 
      const data = req.only('assignment_name', 'created_at', 'period_id', 'teacher', 'type', 'updated_at')
      assignmentData.period_id = period.period_id                        
      
      yield AssignmentList.create(assignmentData) 
      
     
      /*for(student in students){
          const assignment = new StudentAssignment()
          assignment.student_name =  student.student_name 
          assignment.period_id = period.period_id
          assignment.teacher = "Mr. Marshall"
          assignment.assignment_list_id = assignment.period_id
          assignment.student_id = student.student_id
          assignment.student_name = student.student_name
          assignment.assignment_type = assignment.type
          StudentAssignment.create(assignment)
      }*/
      
      res.route('period.assignments.index', {period_id:periodId})
  }
    
/*********************************************************

    Show

*********************************************************/

  * show(req, res) {
        const periods = yield Period.all()
        const periodId = req.param('period_id')
        const assignId = req.param('id')
        const assignment = yield AssignmentList.findOrFail(assignId)
        const lists = yield AssignmentList.query().where('id', assignId).fetch()
        const students = yield Students.query().where('period_id',periodId).orderBy('student_name', 'asc').fetch()
        
        const studAssignments = yield StudentAssignment.query().where('assignment_list_id', assignId).fetch()
        yield res.sendView('assignments.show', { periodId:periodId,lists: lists.toJSON(), students: students.toJSON(), assignment: assignment.toJSON(), periods: periods.toJSON(), studAssignments: studAssignments.toJSON(), assignId:assignId })
  }
    
    
/*********************************************************

    Edit

*********************************************************/

  * edit(req, res) {
    const periods = yield Period.all()
    const assignmentId = req.param('id')
    const periodId = req.param('period_id')
    const assignId = req.param('id')
    const assignment = yield AssignmentList.findOrFail(assignId)
    
    yield res.sendView('assignments.edit', {periods: periods.toJSON(),  periodId:periodId, assignId:assignId, assignment:assignment})
  }
    
/*********************************************************

    Update

*********************************************************/

  * update(req, res) {
      const periodId = req.param('period_id')
      const period = yield Period.findOrFail(periodId)
      const assignmentData = req.only('assignment_name', 'type','teacher') 
      const data = req.only('assignment_name', 'created_at', 'period_id', 'teacher', 'type', 'updated_at')
      
      const assignId = req.param('id')
      const test = req.all()
      //res.json(assignmentData.assignment_name)
      yield Database.table('assignment_lists').where('id', assignId).update(assignmentData)
      yield Database.table('student_assignments')
          .where('assignment_list_id', assignId)
          .update({'assignment_name': assignmentData.assignment_name, 'assignment_type': assignmentData.type})
      
      res.route('period.assignments.index', {period_id:periodId})
  }

    
/*********************************************************

    Destroy

*********************************************************/
  * destroy(req, res) {
        const periods = yield Period.all()
        const periodId = req.param('period_id')
        const assignId = req.param('id')
        const assignment = yield AssignmentList.findOrFail(assignId)
        const lists = yield AssignmentList.query().where('id', assignId).fetch()
        const students = yield Students.query().where('period_id',periodId).orderBy('student_name', 'asc').fetch()
        
        const studAssignments = yield StudentAssignment.query().where('assignment_list_id', assignId).fetch()  
    
    const post = yield AssignmentList.findBy('id', assignId)
    yield post.delete()
    
    yield res.sendView('period.index', {lists: lists.toJSON(), periods: periods.toJSON(),periodId:periodId, assignId:assignId })
    
  }

}

module.exports = AssignmentListController
