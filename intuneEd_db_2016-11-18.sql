# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: intuneEd_db
# Generation Time: 2016-11-18 14:00:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table adonis_schema
# ------------------------------------------------------------

DROP TABLE IF EXISTS `adonis_schema`;

CREATE TABLE `adonis_schema` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `batch` int(11) DEFAULT NULL,
  `migration_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `adonis_schema` WRITE;
/*!40000 ALTER TABLE `adonis_schema` DISABLE KEYS */;

INSERT INTO `adonis_schema` (`id`, `name`, `batch`, `migration_time`)
VALUES
	(1,'1479246974784_students',1,'2016-11-15 17:12:00'),
	(2,'1479246996286_period',1,'2016-11-15 17:12:01'),
	(3,'1479316652342_assignment_lists',2,'2016-11-16 12:24:18'),
	(4,'1479316693701_student_assignments',2,'2016-11-16 12:24:19');

/*!40000 ALTER TABLE `adonis_schema` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assignment_lists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assignment_lists`;

CREATE TABLE `assignment_lists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher` varchar(255) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `assignment_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `assignment_lists` WRITE;
/*!40000 ALTER TABLE `assignment_lists` DISABLE KEYS */;

INSERT INTO `assignment_lists` (`id`, `teacher`, `period_id`, `assignment_name`, `type`, `created_at`, `updated_at`)
VALUES
	(2,'Mr. Marshall',1,'Test2','test',NULL,NULL),
	(3,'Mr. Marshall',3,'Test1','quiz',NULL,NULL),
	(30,'Mr. Marshall',1,'Test #1','Test','2016-11-17 17:36:26','2016-11-17 17:36:26'),
	(44,'Mr. Marshall',1,'Quiz 5','Quiz','2016-11-17 23:58:05','2016-11-17 23:58:05'),
	(45,'Mr. Marshall',1,'Quiz 5','Quiz','2016-11-18 00:00:17','2016-11-18 00:00:17'),
	(46,'Mr. Marshall',5,'Homework 1','Homework','2016-11-18 04:25:33','2016-11-18 04:25:33'),
	(52,'Mr. Marshall',5,'Homework 2','Homework','2016-11-18 04:28:59','2016-11-18 04:28:59'),
	(53,'Mr. Marshall',3,'Blah Blah sheet #12','Homework','2016-11-18 04:33:11','2016-11-18 04:33:11'),
	(54,'Mr. Marshall',3,'Blah Blah sheet #13','Homework','2016-11-18 04:33:59','2016-11-18 04:33:59');

/*!40000 ALTER TABLE `assignment_lists` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table periods
# ------------------------------------------------------------

DROP TABLE IF EXISTS `periods`;

CREATE TABLE `periods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher` varchar(255) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `period_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `periods` WRITE;
/*!40000 ALTER TABLE `periods` DISABLE KEYS */;

INSERT INTO `periods` (`id`, `teacher`, `period_id`, `period_name`, `created_at`, `updated_at`)
VALUES
	(1,'mr. marshall',1,'Period 1',NULL,NULL),
	(3,'mr. marshall',3,'Period 3',NULL,NULL),
	(4,'mr. marshall',4,'Period 4',NULL,NULL),
	(5,'mr. marshall',5,'Period 5',NULL,NULL),
	(6,'mr. marshall',6,'Period 6',NULL,NULL),
	(7,'mr. marshall',7,'Period 7',NULL,NULL);

/*!40000 ALTER TABLE `periods` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table student_assignments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `student_assignments`;

CREATE TABLE `student_assignments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `assignment_list_id` int(11) DEFAULT NULL,
  `assignment_type` varchar(255) DEFAULT NULL,
  `assignment_name` varchar(255) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `student_name` varchar(255) DEFAULT NULL,
  `complete_status` varchar(255) DEFAULT NULL,
  `period_id` int(11) DEFAULT NULL,
  `teacher` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `student_assignments` WRITE;
/*!40000 ALTER TABLE `student_assignments` DISABLE KEYS */;

INSERT INTO `student_assignments` (`id`, `assignment_list_id`, `assignment_type`, `assignment_name`, `student_id`, `student_name`, `complete_status`, `period_id`, `teacher`, `created_at`, `updated_at`)
VALUES
	(255,1,'test','Test',756382,'Jones, Jasmine','no',1,'Mr. Marshall','2016-11-18 04:18:49','2016-11-18 04:18:49'),
	(256,1,'test','Test',531002,'Leslie, Ronald','no',1,'Mr. Marshall','2016-11-18 04:18:49','2016-11-18 04:18:49'),
	(257,1,'test','Test',331500,'Livers, Cory','no',1,'Mr. Marshall','2016-11-18 04:18:49','2016-11-18 04:18:49'),
	(258,1,'test','Test',501383,'Phillips, Natasha','no',1,'Mr. Marshall','2016-11-18 04:18:49','2016-11-18 04:18:49'),
	(259,1,'test','Test',631448,'Ricketts, Breonna','no',1,'Mr. Marshall','2016-11-18 04:18:49','2016-11-18 04:18:49'),
	(260,1,'test','Test',331389,'Robinson, Montrell','no',1,'Mr. Marshall','2016-11-18 04:18:49','2016-11-18 04:18:49'),
	(261,1,'test','Test',460820,'Seay, Shelby','no',1,'Mr. Marshall','2016-11-18 04:18:49','2016-11-18 04:18:49'),
	(262,NULL,NULL,'Test',NULL,NULL,NULL,NULL,'Mr. Marshall','2016-11-18 04:18:49','2016-11-18 04:18:49'),
	(274,52,'Homework','Homework 2',602267,'Adkins, Leah','no',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(275,52,'Homework','Homework 2',401624,'Crick, Randy','yes',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(276,52,'Homework','Homework 2',631242,'Dains, Jaelon	','no',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(277,52,'Homework','Homework 2',661072,'Grapperhaus, Margaret','no',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(278,52,'Homework','Homework 2',501376,'Horrell, Brad','no',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(279,52,'Homework','Homework 2',661250,'Johnson, Haley','yes',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(280,52,'Homework','Homework 2',804539,'Johnson, Mea','no',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(281,52,'Homework','Homework 2',765577,'Norwood, Reginald','yes',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(282,52,'Homework','Homework 2',331520,'Price, Kiley','no',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(283,52,'Homework','Homework 2',302428,'Rankin, Desmand','yes',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(284,52,'Homework','Homework 2',301529,'Riley, Jessica','no',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(285,52,'Homework','Homework 2',302458,'Wheeler, Shelby','no',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(286,52,'Homework','Homework 2',331322,'Rubio Ramirez, Jeffrey','yes',5,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(287,NULL,NULL,'Homework 2',NULL,NULL,NULL,NULL,'Mr. Marshall','2016-11-18 04:32:09','2016-11-18 04:32:09'),
	(288,53,'Homework','Blah Blah sheet #12',460953,'Cary, Quinn','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(289,53,'Homework','Blah Blah sheet #12',530690,'Foreman, Hayden','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(290,53,'Homework','Blah Blah sheet #12',601473,'Forrest, Christian','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(291,53,'Homework','Blah Blah sheet #12',331591,'Harris, Kie\'ara','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(292,53,'Homework','Blah Blah sheet #12',631431,'Harrison, Ashlyn','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(293,53,'Homework','Blah Blah sheet #12',361603,'Kauchak, Nicholas','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(294,53,'Homework','Blah Blah sheet #12',583724,'Parker, Juanye','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(295,53,'Homework','Blah Blah sheet #12',583730,'Peralta, Federico','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(296,53,'Homework','Blah Blah sheet #12',601998,'Pry, Evan','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(297,53,'Homework','Blah Blah sheet #12',661317,'Shannon, Antiawn','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(298,53,'Homework','Blah Blah sheet #12',501389,'Singer, Austin','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(299,53,'Homework','Blah Blah sheet #12',583245,'Steinberger, Nathan','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(300,53,'Homework','Blah Blah sheet #12',804395,'Uppencamp, Henry','no',3,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(301,NULL,NULL,'Blah Blah sheet #12',NULL,NULL,NULL,NULL,'Mr. Marshall','2016-11-18 04:33:17','2016-11-18 04:33:17'),
	(302,54,'Homework','Blah Blah sheet #13',460953,'Cary, Quinn','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(303,54,'Homework','Blah Blah sheet #13',530690,'Foreman, Hayden','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(304,54,'Homework','Blah Blah sheet #13',601473,'Forrest, Christian','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(305,54,'Homework','Blah Blah sheet #13',331591,'Harris, Kie\'ara','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(306,54,'Homework','Blah Blah sheet #13',631431,'Harrison, Ashlyn','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(307,54,'Homework','Blah Blah sheet #13',361603,'Kauchak, Nicholas','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(308,54,'Homework','Blah Blah sheet #13',583724,'Parker, Juanye','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(309,54,'Homework','Blah Blah sheet #13',583730,'Peralta, Federico','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(310,54,'Homework','Blah Blah sheet #13',601998,'Pry, Evan','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(311,54,'Homework','Blah Blah sheet #13',661317,'Shannon, Antiawn','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(312,54,'Homework','Blah Blah sheet #13',501389,'Singer, Austin','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(313,54,'Homework','Blah Blah sheet #13',583245,'Steinberger, Nathan','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(314,54,'Homework','Blah Blah sheet #13',804395,'Uppencamp, Henry','no',3,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(315,NULL,NULL,'Blah Blah sheet #13',NULL,NULL,NULL,NULL,'Mr. Marshall','2016-11-18 04:34:03','2016-11-18 04:34:03'),
	(316,54,'Homework','Blah Blah sheet #13',460953,'Cary, Quinn','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(317,54,'Homework','Blah Blah sheet #13',530690,'Foreman, Hayden','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(318,54,'Homework','Blah Blah sheet #13',601473,'Forrest, Christian','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(319,54,'Homework','Blah Blah sheet #13',331591,'Harris, Kie\'ara','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(320,54,'Homework','Blah Blah sheet #13',631431,'Harrison, Ashlyn','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(321,54,'Homework','Blah Blah sheet #13',361603,'Kauchak, Nicholas','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(322,54,'Homework','Blah Blah sheet #13',583724,'Parker, Juanye','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(323,54,'Homework','Blah Blah sheet #13',583730,'Peralta, Federico','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(324,54,'Homework','Blah Blah sheet #13',601998,'Pry, Evan','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(325,54,'Homework','Blah Blah sheet #13',661317,'Shannon, Antiawn','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(326,54,'Homework','Blah Blah sheet #13',501389,'Singer, Austin','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(327,54,'Homework','Blah Blah sheet #13',583245,'Steinberger, Nathan','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(328,54,'Homework','Blah Blah sheet #13',804395,'Uppencamp, Henry','no',3,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(329,NULL,NULL,'Blah Blah sheet #13',NULL,NULL,NULL,NULL,'Mr. Marshall','2016-11-18 04:34:09','2016-11-18 04:34:09'),
	(330,2,'test','Test2',804266,'Baker, Angela','no',1,'Mr. Marshall','2016-11-18 04:35:35','2016-11-18 04:35:35'),
	(331,2,'test','Test2',331875,'Caldwell-Richie, Ari','no',1,'Mr. Marshall','2016-11-18 04:35:35','2016-11-18 04:35:35'),
	(332,2,'test','Test2',301265,'Hedge, Nathaniel','no',1,'Mr. Marshall','2016-11-18 04:35:35','2016-11-18 04:35:35'),
	(333,2,'test','Test2',756382,'Jones, Jasmine','no',1,'Mr. Marshall','2016-11-18 04:35:35','2016-11-18 04:35:35'),
	(334,2,'test','Test2',531002,'Leslie, Ronald','no',1,'Mr. Marshall','2016-11-18 04:35:35','2016-11-18 04:35:35'),
	(335,2,'test','Test2',331500,'Livers, Cory','no',1,'Mr. Marshall','2016-11-18 04:35:35','2016-11-18 04:35:35'),
	(336,2,'test','Test2',501383,'Phillips, Natasha','no',1,'Mr. Marshall','2016-11-18 04:35:35','2016-11-18 04:35:35'),
	(337,2,'test','Test2',631448,'Ricketts, Breonna','no',1,'Mr. Marshall','2016-11-18 04:35:36','2016-11-18 04:35:36'),
	(338,2,'test','Test2',331389,'Robinson, Montrell','no',1,'Mr. Marshall','2016-11-18 04:35:36','2016-11-18 04:35:36'),
	(339,2,'test','Test2',460820,'Seay, Shelby','no',1,'Mr. Marshall','2016-11-18 04:35:36','2016-11-18 04:35:36'),
	(340,NULL,NULL,'Test2',NULL,NULL,NULL,NULL,'Mr. Marshall','2016-11-18 04:35:36','2016-11-18 04:35:36'),
	(341,30,'Test','Test #1',804266,'Baker, Angela','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(342,30,'Test','Test #1',331875,'Caldwell-Richie, Ari','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(343,30,'Test','Test #1',301265,'Hedge, Nathaniel','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(344,30,'Test','Test #1',756382,'Jones, Jasmine','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(345,30,'Test','Test #1',531002,'Leslie, Ronald','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(346,30,'Test','Test #1',331500,'Livers, Cory','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(347,30,'Test','Test #1',501383,'Phillips, Natasha','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(348,30,'Test','Test #1',331389,'Robinson, Montrell','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(349,30,'Test','Test #1',460820,'Seay, Shelby','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(350,30,'Test','Test #1',631448,'Ricketts, Breonna','no',1,'Mr. Marshall','2016-11-18 04:38:59','2016-11-18 04:38:59'),
	(479,46,'Homework','Homework 1',602267,'Adkins, Leah','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(480,46,'Homework','Homework 1',401624,'Crick, Randy','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(481,46,'Homework','Homework 1',631242,'Dains, Jaelon	','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(482,46,'Homework','Homework 1',661072,'Grapperhaus, Margaret','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(483,46,'Homework','Homework 1',501376,'Horrell, Brad','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(484,46,'Homework','Homework 1',661250,'Johnson, Haley','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(485,46,'Homework','Homework 1',804539,'Johnson, Mea','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(486,46,'Homework','Homework 1',331520,'Price, Kiley','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(487,46,'Homework','Homework 1',302428,'Rankin, Desmand','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(488,46,'Homework','Homework 1',765577,'Norwood, Reginald','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(489,46,'Homework','Homework 1',301529,'Riley, Jessica','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(490,46,'Homework','Homework 1',331322,'Rubio Ramirez, Jeffrey','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44'),
	(491,46,'Homework','Homework 1',302458,'Wheeler, Shelby','no',5,'Mr. Marshall','2016-11-18 07:54:44','2016-11-18 07:54:44');

/*!40000 ALTER TABLE `student_assignments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table students
# ------------------------------------------------------------

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `student_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `student_name` varchar(25) NOT NULL DEFAULT '',
  `period_id` int(1) NOT NULL,
  `class_name` varchar(20) NOT NULL DEFAULT '',
  `grade` int(2) NOT NULL,
  `teacher` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;

INSERT INTO `students` (`student_id`, `student_name`, `period_id`, `class_name`, `grade`, `teacher`)
VALUES
	(301265,'Hedge, Nathaniel',1,'Geometry',12,'mr. marshall'),
	(301523,'Kannapel, Rebecca',6,'Algebra II',10,'mr. marshall'),
	(301529,'Riley, Jessica',5,'Geometry (S1)',11,'mr. marshall'),
	(301571,'Grether, Gabriel',4,'Geometry (S1)',11,'mr. marshall'),
	(301622,'Neuman, Tiffany',7,'Algebra II',10,'mr. marshall'),
	(302000,'Sykes, James',4,'Geometry (S1)',11,'mr. marshall'),
	(302180,'Bedan, Kristofer',6,'Algebra II',10,'mr. marshall'),
	(302268,'Cordell, DellShawn',4,'Geometry (S1)',11,'mr. marshall'),
	(302428,'Rankin, Desmand',5,'Geometry (S1)',11,'mr. marshall'),
	(302458,'Wheeler, Shelby',5,'Geometry (S1)',11,'mr. marshall'),
	(331322,'Rubio Ramirez, Jeffrey',5,'Geometry (S1)',11,'mr. marshall'),
	(331389,'Robinson, Montrell',1,'Geometry',12,'mr. marshall'),
	(331470,'Wilson-Gee, Elizabeth',6,'Algebra II',10,'mr. marshall'),
	(331474,'Overton, Devin',7,'Algebra II',10,'mr. marshall'),
	(331500,'Livers, Cory',1,'Geometry',11,'mr. marshall'),
	(331520,'Price, Kiley',5,'Algebra II',10,'mr. marshall'),
	(331591,'Harris, Kie\'ara',3,'Algebra II (S1)',10,'mr. marshall'),
	(331875,'Caldwell-Richie, Ari',1,'Geometry',12,'mr. marshall'),
	(361361,'Dyke, Crystal',4,'Geometry (S1)',11,'mr. marshall'),
	(361603,'Kauchak, Nicholas',3,'Algebra II (S1)',10,'mr. marshall'),
	(401624,'Crick, Randy',5,'Algebra II',11,'mr. marshall'),
	(460820,'Seay, Shelby',1,'Geometry',12,'mr. marshall'),
	(460953,'Cary, Quinn',3,'Algebra II (S1)',10,'mr. marshall'),
	(501376,'Horrell, Brad',5,'Geometry (S1)',11,'mr. marshall'),
	(501383,'Phillips, Natasha',1,'Geometry',12,'mr. marshall'),
	(501389,'Singer, Austin',3,'Algebra II (S1)',11,'mr. marshall'),
	(501401,'Strickland, Carmen',7,'Algebra II',11,'mr. marshall'),
	(501509,'Hardin, Ora',4,'Geometry (S1)',11,'mr. marshall'),
	(501779,'Boggs, Brittany',7,'Algebra II',11,'mr. marshall'),
	(530690,'Foreman, Hayden',3,'Algebra II (S1)',11,'mr. marshall'),
	(530706,'Robinson, Javier',4,'Geometry (S1)',11,'mr. marshall'),
	(531002,'Leslie, Ronald',1,'Geometry',11,'mr. marshall'),
	(583198,'DeLuna, Carly',4,'Geometry (S1)',12,'mr. marshall'),
	(583228,'Morelos, Apollo',4,'Geometry (S1)',12,'mr. marshall'),
	(583245,'Steinberger, Nathan',3,'Algebra II (S1)',11,'mr. marshall'),
	(583430,'Pulido Ureste, Humbe',4,'Geometry (S1)',11,'mr. marshall'),
	(583431,'Whalen, Gloria',4,'Geometry (S1)',11,'mr. marshall'),
	(583551,'Robbins, Isaiah',6,'Algebra II',11,'mr. marshall'),
	(583724,'Parker, Juanye',3,'Algebra II (S1)',10,'mr. marshall'),
	(583730,'Peralta, Federico',3,'Algebra II (S1)',10,'mr. marshall'),
	(583908,'Danner, Christian',6,'Algebra II',10,'mr. marshall'),
	(584472,'Sanders, Janae',7,'Algebra II',10,'mr. marshall'),
	(601473,'Forrest, Christian',3,'Algebra II (S1)',12,'mr. marshall'),
	(601998,'Pry, Evan',3,'Algebra II (S1)',11,'mr. marshall'),
	(602065,'Carpenter, Eric',6,'Algebra II',11,'mr. marshall'),
	(602111,'Lambert, Jackson',6,'Algebra II',11,'mr. marshall'),
	(602267,'Adkins, Leah',5,'Algebra II',10,'mr. marshall'),
	(603026,'Andrews, Jason',6,'Algebra II',11,'mr. marshall'),
	(631242,'Dains, Jaelon	',5,'Geometry (S1)',12,'mr. marshall'),
	(631287,'Farnsley, Travis',7,'Algebra II',11,'mr. marshall'),
	(631386,'Miller, Jocelyn',4,'Geometry (S1)',11,'mr. marshall'),
	(631431,'Harrison, Ashlyn',3,'Algebra II (S1)',10,'mr. marshall'),
	(631448,'Ricketts, Breonna',1,'Geometry',12,'mr. marshall'),
	(661072,'Grapperhaus, Margaret',5,'Geometry (S1)',11,'mr. marshall'),
	(661126,'Crews, William',6,'Algebra II',12,'mr. marshall'),
	(661250,'Johnson, Haley',5,'Algebra II',10,'mr. marshall'),
	(661317,'Shannon, Antiawn',3,'Algebra II (S1)',11,'mr. marshall'),
	(723566,'Banks, Sophelia',4,'Geometry (S1)',11,'mr. marshall'),
	(756382,'Jones, Jasmine',1,'Geometry',12,'mr. marshall'),
	(765577,'Norwood, Reginald',5,'Geometry (S1)',12,'mr. marshall'),
	(804072,'Neville, Mitchell',4,'Geometry (S1)',12,'mr. marshall'),
	(804266,'Baker, Angela',1,'Geometry',12,'mr. marshall'),
	(804395,'Uppencamp, Henry',3,'Algebra II (S1)',11,'mr. marshall'),
	(804490,'Luna, Sharon',7,'Algebra II',11,'mr. marshall'),
	(804539,'Johnson, Mea',5,'Geometry (S2)',12,'mr. marshall');

/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
