'use strict'

const Schema = use('Schema')

class PeriodTableSchema extends Schema {

  up () {
    this.create('periods', (table) => {
        table.increments()
        table.string('teacher')
        table.integer('period_id')
        table.string('period_name')
        table.timestamps()
    })
  }

  down () {
    this.drop('period')
  }

}

module.exports = PeriodTableSchema
