'use strict'

const Schema = use('Schema')

class StudentAssignmentsTableSchema extends Schema {

  up () {
    this.create('student_assignments', (table) => {
        table.increments()
        table.integer('period_id')
        table.string('teacher')
        table.integer('assignment_list_id')
        table.integer('student_id')
        table.string('student_name')
        table.string('assignment_name')
        table.string('assignment_type')
        table.string('complete_status')
        table.timestamps()
    })
  }

  down () {
    this.drop('student_assignments')
  }

}

module.exports = StudentAssignmentsTableSchema
