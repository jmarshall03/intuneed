'use strict'

const Schema = use('Schema')

class StudentsTableSchema extends Schema {

  up () {
    this.create('students', (table) => {
        table.increments('student_id')
        table.string('student_name')            
        table.integer('period_id')
        table.string('class_name')
        table.integer('grade')
        table.string('teacher')
        table.timestamps()
    })
  }

  down () {
    this.drop('students')
  }

}

module.exports = StudentsTableSchema
