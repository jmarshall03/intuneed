'use strict'

const Schema = use('Schema')

class AssignmentListsTableSchema extends Schema {

  up () {
    this.create('assignment_lists', (table) => {
        table.increments('assignment_id')
        table.string('teacher')
        table.integer('period_id')
        table.string('assignment_name')
        table.string('type')
        table.timestamps()
    })
  }

  down () {
    this.drop('assignment_lists')
  }

}

module.exports = AssignmentListsTableSchema
